# Code review checklist for Peerdom Frontend

## Code
- Scallability
- Readability / Naming / Understandability
- Maintainability
- What would break, if I remove it?
- Is it formalized?
- Will soon change?
- Where it might break?
- Mobile design
- Cover navigation & breadcrumbs
- Types (check for `any`)
- Guards

## Testing
- Cross-app: Map, Projects, Missions, Journal, Salary, Network, Peer
- Cross-view: Map, List, Peer

## Access rights
- Public/private
- Account persmissions
- Admin

## Translation
- Custom term coverage
- Pluralisation coverage
- HTML in translation

## Changelog
- Scope
- Order of the entries
- Working link and formatting

## Metadata
- Name of the issue vs. name of the MR
- Milestone
- Labels
- Description
- Assignee
